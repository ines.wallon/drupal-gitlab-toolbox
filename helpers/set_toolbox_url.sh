#!/bin/bash -
#===============================================================================
#
#          FILE: set_toolbox_url.sh
#
#         USAGE: ./set_toolbox_url.sh
#
#   DESCRIPTION: 
#
#        AUTHOR: Ines WALLON (Missd), ines+bash@famillewallon.com
#       CREATED: 14/05/2024 18:57:07
#      REVISION:  ---
#===============================================================================

templates_path="$(dirname "${BASH_SOURCE[0]}")/../templates"

ls $templates_path
